import 'dart:io';
import 'dart:math';

class Obj {
  int x;
  int y;
  String symbol;

  Obj(this.symbol, this.x, this.y);

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}

class Mole extends Obj {
  int x;
  int y;
  String symbol = "M";

  Mole(this.x, this.y) : super("M", x, y);
}

class Rabbit extends Obj {
  int x;
  int y;
  String symbol = "R";

  Rabbit(this.x, this.y) : super("R", x, y);
}

class TableMap {
  int width;
  int height;
  late Person person;
  bool isFinished = false;
  List<Obj> objects = [];
  int objCount = 0;

  TableMap(this.width, this.height);

  void setPerson(Person person) {
    this.person = person;
    addObj(person);
  }

  void addObj(Obj obj) {
    objects.add(obj);
    objCount++;
  }

  void printSymbolOn(int x, int y) {
    String symbol = '-';
    for (int o = 0; o < objCount; o++) {
      if (objects[o].isOn(x, y)) {
        symbol = objects[o].symbol;
      }
    }
    stdout.write(symbol);
  }

  void showMap() {
    showTitle();
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        printSymbolOn(x, y);
      }
      showNewLine();
    }
  }

  void showTitle() {
    print("Map");
  }

  void showNewLine() {
    print("");
  }

  void showPerson() {
    print(person.symbol);
  }

  bool inMap(int x, int y) {
    // x -> 0-(width-1), y -> 0-(height-1)
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isMole(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Mole && objects[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  bool isRabbit(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Rabbit && objects[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }
}

class Person extends Obj {
  TableMap map;
  late int x;
  late int y;
  int hp = 3;
  int count = 0;
  String symbol = "P";

  Person(this.x, this.y, this.map) : super("P", x, y);

  bool walk(String direction) {
    switch (direction) {
      case 'N':
      case 'w':
        if (walkN()) return false;
        break;
      case 'S':
      case 's':
        if (walkS()) return false;
        break;
      case 'E':
      case 'd':
        if (walkE()) return false;

        break;
      case 'W':
      case 'a':
        if (walkW()) return false;
        break;
      default:
        return false;
    }

    checkRabbit();
    checkMole();
    return true;
  }

  void checkRabbit() {
    if (map.isRabbit(x, y)) {
      //นั้นไม่ใช่ตุ่น
      print("That's not a mole!!!");
      //เลือดที่เหลือ
      hp--;
      print("Current hp = $hp");
      if (hp == 0) {
        map.showMap();
        print("Game over.");
        map.isFinished = true;
      }
    }
  }

  void checkMole() {
    if (map.isMole(x, y)) {
      print("Collect  Moles!!!");
      //นับตัวตุ่น
      count++;
      print("Moles is $count");
      //เจอตุ่นครบมั้ย
      if (count == 10) {
        map.showMap();
        print("You win.");
        print("Congratulation!!!");

        map.isFinished = true;
      }
    }
  }

  bool canWalk(int x, int y) {
    return map.inMap(x, y);
  }

  bool walkW() {
    if (canWalk(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    if (canWalk(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }
}

void addMole(TableMap map, int maxMole) {
  for (int i = 0; i <= maxMole; i++) {
    Mole mole =
        Mole(Random().nextInt(map.width - 1), Random().nextInt(map.height - 1));
    if (map.objects.contains(mole)) {
      i--;
    } else {
      map.addObj(mole);
    }
  }
}

void addRabbit(TableMap map, int maxRabbit) {
  for (int i = 0; i <= maxRabbit; i++) {
    Rabbit rabbit = Rabbit(
        Random().nextInt(map.width - 1), Random().nextInt(map.height - 1));
    if (map.objects.contains(rabbit)) {
      i--;
    } else {
      map.addObj(rabbit);
    }
  }
}

void main(List<String> arguments) {
  TableMap map = TableMap(10, 10);
  Person person = Person(0, 0, map);
  int maxMole = 10;
  int maxRabbit = 5;
  addMole(map, maxMole);
  addRabbit(map, maxRabbit);
  map.setPerson(person);
  while (!map.isFinished) {
    map.showMap();
    // W,a| N,w| E,d| S, s| q: quit
    String direction = stdin.readLineSync()!;
    if (direction == 'q' || direction == 'Q') {
      print("Exit game.");
      break;
    }
    person.walk(direction);
  }
}
